<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Seq_Logging
 *
 * @wordpress-plugin
 * Plugin Name:       Seq Logging
 * Description:       Logs default wordpress warnings, notifications and errors to Seq Server
 * Version:           1.0.0
 * Author:            Aedan McGarry
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-seq-logging
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

function run_seq_logging() {
	$seq_api_url = get_option('seq_api_url');
	if (isset($seq_api_url) && !empty($seq_api_url) && $seq_api_url) {
	  function seq_alert_errors($errno, $errstr, $errfile, $errline) {
	    $errorType = array (
	         E_ERROR                => 'ERROR',
	         E_CORE_ERROR           => 'CORE ERROR',
	         E_COMPILE_ERROR        => 'COMPILE ERROR',
	         E_USER_ERROR           => 'USER ERROR',
	         E_RECOVERABLE_ERROR    => 'RECOVERABLE ERROR',
	         E_WARNING              => 'WARNING',
	         E_CORE_WARNING         => 'CORE WARNING',
	         E_COMPILE_WARNING      => 'COMPILE WARNING',
	         E_USER_WARNING         => 'USER WARNING',
	         E_NOTICE               => 'NOTICE',
	         E_USER_NOTICE          => 'USER NOTICE',
	         E_DEPRECATED           => 'DEPRECATED',
	         E_USER_DEPRECATED      => 'USER_DEPRECATED',
	         E_PARSE                => 'PARSING ERROR'
	    );
	    $errname = array_key_exists($errno, $errorType) ? $errorType[$errno] : 'UNKNOWN ERROR';
	    $level = strpos('WARNING') === false ? (strpos('ERROR') === false ? 'Information' : 'Error') : 'Warning';
	    $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	    $mt = '{errLevel} [{errNo}] {errString} {errFile} on line {errLine}';
	    $datetime =  date("Y-m-d\TH:i:s.000\Z");
	    $eventsString = array(
	      "Events" => array(
	        array(
	          "Timestamp" => $datetime,
	          "Level" => $level,
	          "MessageTemplate" => $mt,
	          "Properties" => array(
	            "errLevel" => $errname,
	            "errNo" => $errno,
	            "errFile" => $errfile,
	            "errString" => $errstr,
	            "errLine" => $errline,
	            "errUrl" => $url,
	            "errorAgent" => $_SERVER['HTTP_USER_AGENT']
	          )
	        )
	      )
	    );
	    $data = json_encode($eventsString);
	    // e.g. "http://docker.for.mac.localhost:5341/api/events/raw";
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,  get_option('seq_api_url'));
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	    $result=@curl_exec($ch);
	  }
	  set_error_handler("seq_alert_errors");
	}
}
run_seq_logging();
