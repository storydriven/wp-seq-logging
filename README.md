Custom wordpress plugin for logging php errors and notification to seq server.  

Add this code to wordpress functions file to created general settings field  
```
add_filter('admin_init', 'my_general_settings_register_fields'); 
function my_general_settings_register_fields() {
  register_setting('general', 'seq_api_url', 'esc_attr');
  add_settings_field('seq_api_url', '<label for="seq_api_url">'.__('Seq Api URL' , 'seq_api_url' ).'</label>' , 'my_general_seq_api_url', 'general');
}

function my_general_seq_api_url() {
  $seq_api_url = get_option( 'seq_api_url', '' );
  echo '<input id="seq_api_url" style="width: 35%;" type="text" name="seq_api_url" value="' . $seq_api_url . '" />';
}
```
